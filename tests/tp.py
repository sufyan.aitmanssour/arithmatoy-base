# Aucun n'import ne doit être fait dans ce fichier


def nombre_entier(n: int) -> str:
    if n == 0:
        return "0"
    else:
        lettre_s = "S" * n + "0"
        return lettre_s


def S(n: str) -> str:
    return "S" + n

def addition(a: str, b: str) -> str:
    if a == "0":
        return b
    elif b == "0":
        return a
    else:
        return S(addition(a[1:], b))
   
    
def multiplication(a: str, b: str) -> str:
    if a == "0" or b == "0":
        return "0"
    elif a.startswith("S"):
        return addition(b, multiplication(a[1:], b))
    else:
        return addition(a, multiplication(a, b[1:]))


def facto_ite(n: int) -> int:
    if n == 0:
        return 1
    else:
        result = 1
        for i in range(1, n+1):
            result *= i
        return result


def facto_rec(n: int) -> int:
    if n == 1 or n == 0:
        return 1
    else:
        return n * facto_rec(n - 1)


def fibo_rec(n: int) -> int:
    if n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        return fibo_rec(n-1) + fibo_rec(n-2)


def fibo_ite(n: int) -> int:
    if n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        a, b = 0, 1
        for i in range(2, n+1):
            a, b = b, a + b
        return b


def golden_phi(n: int) -> int:
    return fibo_ite(n + 1)/fibo_ite(n)


def sqrt5(n: int) -> float:
    return golden_phi(n+1) / 2

def pow(a: float, n: int) -> float:
    return a ** n